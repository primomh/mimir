//
//  Document.swift
//  MimiriOS
//
//  Created by Liberitus on 09.05.18.
//  Copyright © 2018 Primault. All rights reserved.
//

import Foundation
import UIKit
import PDFKit

struct Document: Equatable {
    let identifier: String
    let name: String
    let path: URL
    let creationDate: Date
    var thumbnail: UIImage {
        guard let data = try? Data(contentsOf: path),
            let page = PDFDocument(data: data)?.page(at: 0) else {
                return UIImage()
        }
        
        let pageSize = page.bounds(for: .mediaBox)
        let pdfScale = 240 / pageSize.width
        
        // Apply if you're displaying the thumbnail on screen
        let scale = UIScreen.main.scale * pdfScale
        let screenSize = CGSize(width: pageSize.width * scale, height: pageSize.height * scale)
        
        return page.thumbnail(of: screenSize, for: .mediaBox)
    }
    
    
}
