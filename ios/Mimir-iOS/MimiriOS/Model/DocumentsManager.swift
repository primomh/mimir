//
//  DocumentManager.swift
//  MimiriOS
//
//  Created by Liberitus on 08.05.18.
//  Copyright © 2018 Primault. All rights reserved.
//

import Foundation
import os

class DocumentsManager {
    
    var count: Int { return documents.count }
    
    private var documents: [Document] = []
    
    func allDocuments() -> [Document] {
        return documents;
    }
    
    func add(_ document: Document) {
        if !documents.contains(document) {
            documents.append(document)
        }
    }
    
    func add(file: URL) {
        do {
            let fileManager = FileManager.default
            let path = kPath.document.appendingPathComponent(file.lastPathComponent)
            try fileManager.moveItem(at: file, to: path)
            let doc = Document(identifier: UUID().uuidString, name: file.lastPathComponent, path: path, creationDate: Date())
            add(doc)
        } catch {
            let message = "Add file document failed:\(error)"
            os_log("%@", log:kLog.storage, type:.error, message)
        }
    }
    
    func remove(_ document: Document) {
        if let index = documents.index(of: document) {
            documents.remove(at: index)
        }
    }

}
