//
//  DocumentPresenter.swift
//  MimiriOS
//
//  Created by Liberitus on 25.05.18.
//  Copyright © 2018 Primault. All rights reserved.
//

import Foundation
import os

protocol DocumentsViewable {
    func refreshDocuments(documents: [Document])
}

protocol DocumentsPresentable {
    init(view:DocumentsViewable)
    func reloadDocuments()
}

class DocumentsPresenter: DocumentsPresentable {
    
    let view: DocumentsViewable!
    let documentManager: DocumentsManager!
    
    required init(view: DocumentsViewable) {
        self.view = view
        self.documentManager = DocumentsManager()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadImportedDocument(_:)), name: kNotification.NewImportedDocumentNotification, object: nil)
    }
    
    func reloadDocuments() {
        view.refreshDocuments(documents: documentManager.allDocuments())
    }
    
    @objc func loadImportedDocument(_ notification: Notification) {        
        if let url = notification.userInfo?["url"] as? URL {
            documentManager.add(file: url)
            reloadDocuments()
        }
    }
}
