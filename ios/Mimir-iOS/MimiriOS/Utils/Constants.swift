//
//  Constants.swift
//  MimiriOS
//
//  Created by Liberitus on 11.10.18.
//  Copyright © 2018 Primault. All rights reserved.
//

import Foundation
import os

struct kNotification {
    static let NewImportedDocumentNotification = Notification.Name("NewImportedDocumentNotification")
}

struct kPath {
    static let document = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    static let tmpInbox = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true).appendingPathComponent("Inbox")
}

struct kLog {
    static let main = OSLog(subsystem:Bundle.main.bundleIdentifier!, category:"Default")
    static let storage = OSLog(subsystem:Bundle.main.bundleIdentifier!, category:"Storage")
}
