//
//  DocumentViewController.swift
//  MimiriOS
//
//  Created by Liberitus on 04.10.18.
//  Copyright © 2018 Primault. All rights reserved.
//

import UIKit
import PDFKit

class DocumentViewController: UIViewController {
    
    var pdfDocument: PDFDocument!
    private var pdfView: PDFView!

    override func viewDidLoad() {
        super.viewDidLoad()

        pdfView = PDFView()
        pdfView.autoScales = true
        
        view.addSubview(pdfView)
        pdfView.translatesAutoresizingMaskIntoConstraints = false
        pdfView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        pdfView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        pdfView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        pdfView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pdfView.document = pdfDocument
        pdfView.goToFirstPage(nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
