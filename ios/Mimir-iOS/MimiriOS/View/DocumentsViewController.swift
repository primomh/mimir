 //
//  DocumentsViewController.swift
//  MimiriOS
//
//  Created by Liberitus on 27.07.18.
//  Copyright © 2018 Primault. All rights reserved.
//

import UIKit
import PDFKit
 
class DocumentsViewController: UICollectionViewController, DocumentsViewable {
    
    @IBOutlet var dataProvider: DocumentsDataProvider!
    
    var presenter: DocumentsPresenter!
    var documents: [Document] = [] { didSet {
        dataProvider.documents = documents
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = DocumentsPresenter(view: self)
        presenter.reloadDocuments()

    }
    
    // MARK: DocumentsViewable
    func refreshDocuments(documents: [Document]) {
        self.documents = documents
        self.collectionView?.reloadData()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDocumentVC" {
            let document = (sender as! DocumentCell).document
            let documentVC = segue.destination as! DocumentViewController
            documentVC.pdfDocument = PDFDocument(url: (document?.path)!)
            documentVC.title = document?.name
        }
    }
    
}
