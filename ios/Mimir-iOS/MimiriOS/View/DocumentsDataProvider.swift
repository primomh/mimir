//
//  DocumentsDataSource.swift
//  MimiriOS
//
//  Created by Liberitus on 27.09.18.
//  Copyright © 2018 Primault. All rights reserved.
//

import UIKit

class DocumentsDataProvider: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var documents: [Document]?
    private let reuseIdentifier = "DocumentCellIdentifier"
    
    //MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let documents = documents else {
            return 0
        }
        
        return documents.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! DocumentCell
        guard let documents = documents else {
            cell.backgroundColor = UIColor.black
            return cell
        }
        
        cell.configure(with: documents[indexPath.row]) 
        return cell
    }
}
