//
//  DocumentCell.swift
//  MimiriOS
//
//  Created by Liberitus on 01.10.18.
//  Copyright © 2018 Primault. All rights reserved.
//

import UIKit

class DocumentCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    var document: Document!
    
    func configure(with document:Document) {
        self.document = document
        imageView.image = document.thumbnail
        titleLabel.text = document.name
    }
    
    
}
