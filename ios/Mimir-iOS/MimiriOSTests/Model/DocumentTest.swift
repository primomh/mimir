//
//  DocumentTest.swift
//  MimiriOSTests
//
//  Created by Liberitus on 16.05.18.
//  Copyright © 2018 Primault. All rights reserved.
//

import XCTest
@testable import MimiriOS

class DocumentTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_Init_TakesNamePathAndCreationDate() {
        let path = URL(fileURLWithPath: "bar")
        let document = Document(identifier:"1", name: "Foo", path: path, creationDate: Date())
        
        XCTAssertNotNil(document, "Document should not be nil")
    }
    
}
