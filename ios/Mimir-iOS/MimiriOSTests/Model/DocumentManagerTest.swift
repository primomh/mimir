//
//  DocumentManagerTest.swift
//  Mimir-iOSTests
//
//  Created by Liberitus on 07.05.18.
//  Copyright © 2018 Primault. All rights reserved.
//

import XCTest
@testable import MimiriOS

class DocumentManagerTest: XCTestCase {
    
    var sut: DocumentsManager!
    
    override func setUp() {
        super.setUp()
        sut = DocumentsManager()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_Count_Initially_IsZero() {
        XCTAssertEqual(sut.count, 0)
    }
    
    func test_Add_IncreaseDocumentCountToOne() {
        let document = Document(identifier: "1", name: "Foo", path: URL(fileURLWithPath: "Bar"), creationDate: Date())
        sut.add(document)
        
        XCTAssertEqual(sut.count, 1)
    }
    
    func test_Remove_DecreaseDocumentCountToZero() {
        let document = Document(identifier: "1", name: "Foo", path: URL(fileURLWithPath: "Bar"), creationDate: Date())
        sut.add(document)
        XCTAssertEqual(sut.count, 1)
        
        sut.remove(document)
        XCTAssertEqual(sut.count, 0)
    }
    
    func test_Add_WhenDocumentIsAlreadyAdded_DoesNotIncrementCount() {
        let date = Date()
        sut.add(Document(identifier: "1", name: "Foo", path: URL(fileURLWithPath: "Bar"), creationDate: date))
        sut.add(Document(identifier: "1", name: "Foo", path: URL(fileURLWithPath: "Bar"), creationDate: date))
        
        XCTAssertEqual(sut.count, 1)
    }
    
    func test_Remove_WhenDocumentIsNotPresent_DoesNotDecreaseCount() {
        sut.add(Document(identifier: "1", name: "Foo", path: URL(fileURLWithPath: "Bar"), creationDate: Date()))
        XCTAssertEqual(sut.count, 1)
        
        sut.remove(Document(identifier: "2", name: "Bar", path: URL(fileURLWithPath: "Foo"), creationDate: Date()))
        XCTAssertEqual(sut.count, 1)
    }
    
    func test_AllDocuments_GetAllDocuments() {
        let doc1 = Document(identifier: "1", name: "Foo", path: URL(fileURLWithPath: "Bar"), creationDate: Date())
        let doc2 = Document(identifier: "2", name: "Bar", path: URL(fileURLWithPath: "Foo"), creationDate: Date())
        sut.add(doc1)
        sut.add(doc2)
        
        let allDocuments = sut.allDocuments()
        XCTAssertTrue(allDocuments.contains(doc1))
        XCTAssertTrue(allDocuments.contains(doc2))
    }
    
    func test_Add_WithNameAndData_DocumentAdded() {
        let name = "Foo"
        let data = "Bar".data(using: .utf8)
        let path = kPath.tmpInbox.appendingPathComponent(name)
        do {
            try data?.write(to: path)
        } catch {}
        
        sut.add(file: path)
        XCTAssertEqual(sut.count, 1)
    }
}
