//
//  DocumentPresenterTest.swift
//  MimiriOSTests
//
//  Created by Liberitus on 25.05.18.
//  Copyright © 2018 Primault. All rights reserved.
//

import XCTest
@testable import MimiriOS

class DocumentPresenterTest: XCTestCase {
    
    var testDocumentsViewController: TestDocumentsViewController!
    var sut: DocumentsPresenter!
    
    override func setUp() {
        super.setUp()
        cleanDirectories()
        do {
            try FileManager.default.createDirectory(at: kPath.tmpInbox, withIntermediateDirectories: true, attributes: nil)
        } catch {}
        
        testDocumentsViewController = TestDocumentsViewController()
        sut = DocumentsPresenter(view:testDocumentsViewController)
    }
    
    override func tearDown() {
        cleanDirectories()
        super.tearDown()
    }
    
    func test_Init_SetViewRespondingToDocumentViewProtocol() {
        XCTAssertNotNil(sut);
    }
    
    func test_ReloadDocuments_GetDocumentsAndRefreshView() {
        let doc1 = Document(identifier:"1", name: "Foo", path: URL(fileURLWithPath: "Bar"), creationDate: Date())
        sut.documentManager.add(doc1)
        sut.reloadDocuments()
        
        XCTAssertTrue(testDocumentsViewController.isRefreshed)
        XCTAssertTrue((testDocumentsViewController.documents?.contains(doc1))!)
    }
    
    func test_Handle_NewImportedDocumentNotification_AddFile () {
        let name = "Foo"
        let data = "Bar".data(using: .utf8)
        let path = kPath.tmpInbox.appendingPathComponent(name)
        do {
            try data?.write(to: path)
        } catch {}
        
        NotificationCenter.default.removeObserver(((UIApplication.shared.keyWindow?.rootViewController as! UINavigationController).topViewController as! DocumentsViewController).presenter)
        
        NotificationCenter.default.post(name: kNotification.NewImportedDocumentNotification, object: self, userInfo: ["url": path])
        
        XCTAssertEqual(sut.documentManager.count, 1)
    }
    
    //MARKS: Utils
    
    func cleanDirectories() {
        do {
            let fileManager = FileManager.default
            try fileManager.removeItem(at: kPath.tmpInbox)
            for content in try fileManager.contentsOfDirectory(at: kPath.document, includingPropertiesForKeys: nil, options: .skipsHiddenFiles) {
                try fileManager.removeItem(at: content)
            }
        } catch {}
    }
    
}

class TestDocumentsViewController: DocumentsViewable {
    
    var documents: [Document]?
    
    var isRefreshed = false
    
    func refreshDocuments(documents: [Document]) {
        isRefreshed = true
        self.documents = documents
    }
}
